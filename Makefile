#----------------------------- Makefile ----------------------------------

SRC = pipex.c ft_utils.c
OBJS = $(SRC:.c=.o)
NAME = pipex
CFLAGS = -Wall -Werror -Wextra
CC = gcc

%.o: %.c
	$(CC) $(CFLAGS) -c $^

all: $(NAME)

re: fclean all

$(NAME): $(OBJS) libft.a ./includes/pipex.h
	$(CC) $(CFLAGS) -Llibft -lft -o $(NAME) $(OBJS)

bonus:
	make -C ./bonus
	mv ./bonus/pipex ./pipex

libft.a:
	make -C libft
clean:
	rm -rf *.o
	make clean -C libft
	make clean -C ./bonus

fclean: clean
	rm -rf $(NAME)
	make fclean -C libft
	make fclean -C ./bonus

.PHONY: bonus clean fclean re all
