/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex_bonus.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/03 01:43:16 by anramire          #+#    #+#             */
/*   Updated: 2022/06/07 22:24:15 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "../includes/pipex.h"
#include <fcntl.h>

void	child_1_process(char *argv[], char *envp[], int pipe[2]);
void	child_2_process(int argc, char *argv[], char *envp[], int pipe[2]);
void	call_up_to_6_arguments(int argc, char *argv[], char *envp[]);
void	call_5_arguments(int argc, char *argv[], char *envp[]);

int	main(int argc, char *argv[], char *envp[])
{
	if (argc >= 6)
		call_up_to_6_arguments(argc, argv, envp);
	if (argc == 5)
		call_5_arguments(argc, argv, envp);
	return (0);
}

void	child_1_process(char *argv[], char *envp[], int pipe[2])
{
	char	*program;
	char	**arg;
	int		file_d;

	program = get_path(envp, argv[2]);
	arg = ft_split(argv[2], ' ');
	if (access(argv[1], R_OK) != 0)
	{
		free(arg);
		perror("file not found: ");
		exit(1);
	}
	file_d = open(argv[1], O_RDONLY);
	dup2(file_d, 0);
	close(file_d);
	close(pipe[0]);
	dup2(pipe[1], 1);
	close(pipe[1]);
	if (execve(program, arg, envp) < 0)
		check_exec(arg);
	exit(1);
}

void	child_2_process(int argc, char *argv[], char *envp[], int pipe[2])
{
	char	*program;
	char	**arg;
	int		file_d;

	program = get_path(envp, argv[argc - 2]);
	arg = ft_split(argv[argc - 2], ' ');
	file_d = open(argv[argc - 1], O_CREAT | O_WRONLY | O_TRUNC, 0644);
	if (file_d == -1)
	{
		free(arg);
		free(program);
		perror("Could not create this file or repository: ");
		exit(EXIT_FAILURE);
	}
	close(pipe[1]);
	dup2(pipe[0], 0);
	close(pipe[0]);
	dup2(file_d, 1);
	if (execve(program, arg, envp) < 0)
		check_exec(arg);
	close(file_d);
	exit(1);
}

void	call_up_to_6_arguments(int argc, char *argv[], char *envp[])
{
	int	**fds;
	int	son1;
	int	son2;
	int	i;

	fds = get_fds(argc);
	pipe(fds[0]);
	son1 = fork();
	check_error(son1);
	if (son1 == 0)
		child_1_process(argv, envp, fds[0]);
	close(fds[0][1]);
	i = do_loop(argc, argv, envp, fds);
	son2 = fork();
	check_error(son2);
	if (son2 == 0)
		child_2_process(argc, argv, envp, fds[i]);
	free_fds(argc, fds);
}

void	call_5_arguments(int argc, char *argv[], char *envp[])
{
	int	fd[2];
	int	son1;
	int	son2;

	pipe(fd);
	son1 = fork();
	check_error(son1);
	if (son1 == 0)
		child_1_process(argv, envp, fd);
	son2 = fork();
	check_error(son2);
	if (son2 == 0)
		child_2_process(argc, argv, envp, fd);
}
