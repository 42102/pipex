/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils2_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/07 21:40:35 by anramire          #+#    #+#             */
/*   Updated: 2022/06/07 22:24:56 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/pipex.h"

void	check_error(int status)
{
	if (status < 0)
	{
		perror("error pipex: ");
		exit(-1);
	}
}

void	check_exec(char **arg)
{
	free(arg);
	perror("pipex failure: ");
	exit(EXIT_FAILURE);
}
