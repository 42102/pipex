/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils_bonus.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/06 16:28:35 by anramire          #+#    #+#             */
/*   Updated: 2022/06/07 21:49:22 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/pipex.h"

char	*get_path(char *envp[], char *cmd)
{
	int		i;
	char	*path;
	char	**paths;
	char	**command;

	i = 0;
	while (ft_strncmp(envp[i], "PATH=", 5) != 0)
		i++;
	path = ft_substr(envp[i], 5, ft_strlen(envp[i]) - 4);
	paths = ft_split(path, ':');
	i = 0;
	command = ft_split(cmd, ' ');
	cmd = command[0];
	while (paths[i] != NULL)
	{
		paths[i] = ft_strjoin(paths[i], "/");
		paths[i] = ft_strjoin(paths[i], cmd);
		if (access(paths[i], X_OK) == 0)
			return (paths[i]);
		i++;
	}
	free(paths);
	free(path);
	return (NULL);
}

int	**get_fds(int argc)
{
	int	**fds;
	int	i;

	i = 0;
	argc = argc - 4;
	fds = (int **)malloc(argc * sizeof(int *));
	while (i < argc)
	{
		fds[i] = (int *)malloc(2 * sizeof(int));
		i++;
	}
	return (fds);
}

void	free_fds(int argc, int **fds)
{
	int	i;

	argc = argc - 4;
	i = 0;
	while (i < argc)
	{
		free(fds[i]);
		i++;
	}
	free(fds);
}

int	do_loop(int argc, char *argv[], char *envp[], int **fds)
{
	int	i;
	int	son;

	i = 0;
	while (i < (argc - 5))
	{
		close(fds[i][1]);
		pipe(fds[i + 1]);
		son = fork();
		check_error(son);
		if (son == 0)
			child_3_process(argv[3 + i], envp, fds[i], fds[i + 1]);
		i++;
	}
	return (i);
}

void	child_3_process(char *argv, char *envp[], int pipe1[2], int pipe2[2])
{
	char	*program;
	char	**arg;

	program = get_path(envp, argv);
	arg = ft_split(argv, ' ');
	dup2(pipe1[0], 0);
	close(pipe1[1]);
	dup2(pipe2[1], 1);
	close(pipe2[0]);
	if (execve(program, arg, envp) < 0)
	{
		free(arg);
		perror("pipex failure: ");
		exit(EXIT_FAILURE);
	}
	close(pipe1[0]);
	close(pipe2[1]);
	exit(1);
}
