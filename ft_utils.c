/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/06 16:28:35 by anramire          #+#    #+#             */
/*   Updated: 2022/06/06 22:29:46 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/pipex.h"

char	*get_path(char *envp[], char *cmd)
{
	int		i;
	char	*path;
	char	**paths;
	char	**command;

	i = 0;
	while (ft_strncmp(envp[i], "PATH=", 5) != 0)
		i++;
	path = ft_substr(envp[i], 5, ft_strlen(envp[i]) - 4);
	paths = ft_split(path, ':');
	i = 0;
	command = ft_split(cmd, ' ');
	cmd = command[0];
	while (paths[i] != NULL)
	{
		paths[i] = ft_strjoin(paths[i], "/");
		paths[i] = ft_strjoin(paths[i], cmd);
		if (access(paths[i], X_OK) == 0)
			return (paths[i]);
		i++;
	}
	free(paths);
	free(path);
	return (NULL);
}
