/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/06 16:24:28 by anramire          #+#    #+#             */
/*   Updated: 2022/06/07 22:33:17 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H
# include <stdlib.h>
# include "../libft/libft.h"
# include <stdio.h>

char	*get_path(char *envp[], char *cmd);
int		**get_fds(int argc);
void	free_fds(int argc, int **fds);
void	check_error(int status);
void	child_3_process(char *argv, char *envp[], int pipe1[2], int pipe2[2]);
int		do_loop(int argc, char *argv[], char *envp[], int **fds);
void	check_exec(char **arg);

#endif
