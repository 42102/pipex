/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/03 01:43:16 by anramire          #+#    #+#             */
/*   Updated: 2022/06/06 22:35:38 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft/libft.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "includes/pipex.h"
#include <fcntl.h>

void	child_1_process(char *argv[], char *envp[], int pipe[2]);
void	check_error(int status);
void	child_2_process(char *argv[], char *envp[], int pipe[2]);

int	main(int argc, char *argv[], char *envp[])
{
	int	fd[2];
	int	son1;
	int	son2;

	if (argc == 5)
	{
		pipe(fd);
		son1 = fork();
		check_error(son1);
		if (son1 == 0)
			child_1_process(argv, envp, fd);
		son2 = fork();
		check_error(son2);
		if (son2 == 0)
			child_2_process(argv, envp, fd);
	}
	return (0);
}

void	child_1_process(char *argv[], char *envp[], int pipe[2])
{
	char	*program;
	char	**arg;
	int		file_d;

	program = get_path(envp, argv[2]);
	arg = ft_split(argv[2], ' ');
	if (access(argv[1], R_OK) != 0)
	{
		free(arg);
		perror("file not found: ");
		exit(1);
	}
	file_d = open(argv[1], O_RDONLY);
	dup2(file_d, 0);
	close(pipe[0]);
	dup2(pipe[1], 1);
	if (execve(program, arg, envp) < 0)
	{
		free(arg);
		perror("pipex failure: ");
		exit(127);
	}
	close(file_d);
	close(pipe[1]);
	exit(1);
}

void	check_error(int status)
{
	if (status < 0)
	{
		perror("error pipex: ");
		exit(-1);
	}
}

void	child_2_process(char *argv[], char *envp[], int pipe[2])
{
	char	*program;
	char	**arg;
	int		file_d;

	program = get_path(envp, argv[3]);
	arg = ft_split(argv[3], ' ');
	file_d = open(argv[4], O_CREAT | O_WRONLY | O_TRUNC, 0644);
	if (file_d == -1)
	{
		free(arg);
		free(program);
		perror("Could not create this file or repository: ");
		exit(1);
	}
	close(pipe[1]);
	dup2(pipe[0], 0);
	dup2(file_d, 1);
	if (execve(program, arg, envp) < 0)
	{
		free(arg);
		perror("pipex failure: ");
		exit(127);
	}
	close(file_d);
	exit(1);
}
